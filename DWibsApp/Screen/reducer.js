export default(state='empty', action) =>{
  switch(action.type){
    case'LOGGED':
      return action.id;
    case'LOGOUT':
      return 'empty';
    default:
      return state;
  }
}