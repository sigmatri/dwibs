import React, { Component } from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';
import * as Font from 'expo-font';
import { FontAwesome5 } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import {connect} from 'react-redux';
const DEVICE = Dimensions.get('window')
const removeId = ()=>{
  return{
    type:'LOGOUT'
  };
}


class ProfileScreen extends Component{
  constructor(props){
    super(props);
    this.state={
      user: [],
      fontLoaded: false,
      pleaseReRender:false,
      username:'username'
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/MontserratAlternates-Regular.ttf'),
      'montserrat-extra-bold': require('./assets/fonts/MontserratAlternates-ExtraBold.ttf'),
      'montserrat-bold': require('./assets/fonts/MontserratAlternates-Bold.ttf'),
      'montserrat-semi-bold': require('./assets/fonts/MontserratAlternates-SemiBold.ttf'),
      'montserrat-medium': require('./assets/fonts/MontserratAlternates-Medium.ttf'),
    });
    this.setState({ fontLoaded:true});
    this.fetchData();
  }

  fetchData = async()=>{
    console.log("============")
    console.log(this.props.id)
    let api = `https://dwibs-686cb.firebaseio.com/users/${this.props.id}.json`
    const response = await fetch(api)
      .then(sresponse=>{
        return sresponse.json();
      })
      .then(user =>{
        console.log(user);
        this.setState({user: user, pleaseUpdate:false});
        this.setState({
          username: user.username,
        })
      })
      .catch(err =>{
        console.log("error----", err);
      })      
  }


  changeHeader = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 24,
        fontWeight: '600',
        color:'#FFFFFF',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 40
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  changeNavText = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#BB86FC',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  changeNavText_1 = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#A9A9A9',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  changeTextStyle = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        fontWeight: '600',
        color:'#000000',
        fontFamily : 'montserrat',
        marginLeft:5,
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  changeTextStyleSinonpsis = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"white",
        fontSize:16,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  changeTextStyleSinonpsisTittle = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"#FF7597",
        fontSize:32,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  changeTextButton = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"white",
        fontSize:18,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }
  changeUserNameStyle = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"white",
        fontSize:24,
        textAlign:'justify',
        marginLeft:10,
        marginTop:10
      }
    }else{
      return{
        fontSize: 24,
      }
    }
  }

  greenText(){
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"#00A707",
        fontSize:20,
        textAlign:'justify',
        marginTop:10
      }
    }else{
      return{
        fontSize: 20,
      }
    }
  }

  blueText(){
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"#4898E1",
        fontSize:20,
        textAlign:'justify',
        marginTop:10
      }
    }else{
      return{
        fontSize: 20,
      }
    }
  }

  whiteText(){
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"white",
        fontSize:18,
        textAlign:'justify',
        margin :10
      }
    }else{
      return{
        fontSize: 20,
      }
    }
  }


  logoutHandler=()=>{
    this.props.removeId();
    this.props.navigation.popToTop();
  }
  render(){
    console.log(this.props.id)
    return(
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.TabBar}>
          <Image source={require('./assets/background.png')} style={styles.background_image} blurRadius={4}/>
            <View style={styles.headerContainer}>
              <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                <Ionicons name="ios-arrow-back" size={35} color="white" style={{marginLeft:20, marginTop:15}} />
              </TouchableOpacity>
              <View style={styles.pp_container}>
                <Image source={require('./assets/pp.png')} style={styles.pp_image} />
                <Text style={this.changeUserNameStyle()}>{this.state.username}</Text>
                <View style={{flex:1, flexDirection:'column-reverse', justifyContent:'space-between', alignItems:'center'}}>
                  <TouchableOpacity style={styles.buttonLogout} onPress={()=>{this.logoutHandler()}}>
                    <Text style={this.changeTextButton()}>Logout</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.body}>
            <View style={styles.currentContainer}>
              <View style={{flexDirection:'row', justifyContent:'space-between', marginHorizontal:10}}>
                <Text style={this.greenText()}>Watching</Text>
                <Text style={this.greenText()}>Reading</Text>
              </View>
              <Text style={this.whiteText()}> you dont have any item to read or watch </Text>
            </View>
            <View style={styles.planWatchContainer}>
              <TouchableOpacity style={{flexDirection:'row'}}>
                <Text style={this.blueText()}> Plan to Watch </Text>
                <FontAwesome5 name="play" size={15} color="green" style={{marginTop:12}}/>
              </TouchableOpacity>
              <Text style={this.whiteText()}> empty item </Text>
            </View>
            <View style={styles.planReadContainer}>
              <TouchableOpacity style={{flexDirection:'row'}}>
                <Text style={this.blueText()}> Plan to Read </Text>
                <FontAwesome5 name="play" size={15} color="green" style={{marginTop:12}}/>
              </TouchableOpacity>
              <Text style={this.whiteText()}> empty item </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#121212'
  },
  TabBar:{
    height:DEVICE.height * 0.45,
  },
  background_image:{
    width:'100%', 
    height:DEVICE.height * 0.35,
    marginBottom:-DEVICE.height * 0.35,
  },
  headerContainer:{
    height:DEVICE.height * 0.35,
    justifyContent:'space-between',
  },
  pp_container:{
    flexDirection:'row',
    marginTop:DEVICE.height * 0.20
  },
  pp_image:{
    width: DEVICE.width*0.25,
    height: DEVICE.width*0.25,
    marginLeft:20,
    borderRadius:5,
  },
  buttonLogout:{
    width:100,
    height:30,
    backgroundColor:'#F82D2D',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
  },

})
function mapStateToProps(state){
  return{
    id: state
  }
}
export default connect (mapStateToProps, {removeId})(ProfileScreen)