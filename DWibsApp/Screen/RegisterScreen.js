import React, { Component } from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import * as Font from 'expo-font';
import * as firebase from 'firebase';

const DEVICE = Dimensions.get('window')


export default class RegisterScreen extends Component{
  constructor(){
    super();
    this.state={
      fontLoaded:false,
      userName: '',
      password: '',
      rePassword: '',
      isError: false,
      users: [],
      errorMsg:'',
      isRegisterSuccess:false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/MontserratAlternates-Regular.ttf'),
      'montserrat-bold': require('./assets/fonts/MontserratAlternates-ExtraBold.ttf'),
    });
    this.setState({ fontLoaded:true});
  }

  writeUserData = async(username,password)=>{
    await firebase.database().ref('users/').push({
        username,
        password
    }).then((data)=>{
        //success callback
        this.setState({isRegisterSuccess : true})
        console.log(this.state.isRegisterSuccess)
        console.log('data ' , data)
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
  }

  registerHandler = async()=>{
    this.props.pleaseUpdate;
    console.log("redux",this.props.isUpdate);
    console.log(this.props);

    console.log(this.state.userName +' '+this.state.password+' '+this.state.rePassword)
    if(this.state.userName ==''){
      this.setState({errorMsg : 'usename tidak boleh kosong'});
      this.setState({isError : true});
    }else{
      if(this.state.password==this.state.rePassword){
        console.log("masuk 3")
        //push data to firebase
        await this.writeUserData(this.state.userName, this.state.password)
        this.setState({isError : false});
        //kalo berhasil navigate ke Login.
        console.log("11 ",this.state.isRegisterSuccess)
        if(this.state.isRegisterSuccess==true){
          console.log(this.props.pleaseUpdate);
          this.props.pleaseUpdate;
          console.log("redux",this.props.isUpdate);
          this.props.navigation.navigate('Login', {update:true})
          console.log("berhasil register")
        }else{
          this.setState({errorMsg : 'register gagal, coba lakukan lagi'});
          this.setState({isError : true});
        }
        
      }else{
        this.setState({errorMsg : 'password tidak sama'});
        this.setState({isError : true});
        console.log("masuk 2")
      }
    }
  }

  changeFontInputText=()=>{
    if(this.state.fontLoaded){
      return{
        width: DEVICE.width*0.9,
        height: 65,
        padding: 20,
        fontSize: 18,
        fontWeight: '600',
        backgroundColor: 'rgba(255, 194, 194, 0.6)',
        marginVertical:5,
        fontFamily : 'montserrat',
      }
    }else{
      return{
        width: DEVICE.width*0.9,
        height: 65,
        padding: 20,
        fontSize: 18,
        fontWeight: '600',
        backgroundColor: 'rgba(255, 194, 194, 0.6)',
        marginVertical:10,
      }
    }
  }

  changeFontTextButton=()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 14,
        fontWeight: '600',
        color:'blue',
        fontFamily : 'montserrat',
      }
    }else{
      return{
        fontSize: 14,
        fontWeight: '600',
      }
    }
  }

  changeFontButton=()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        color:'black',      
        fontFamily : 'montserrat-bold',
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={styles.container} behavior={"height"} enabled>
        <ScrollView>
          <View style={styles.background}>
            <Image source={require('./assets/background.png')} style={styles.background_image}/>
            <View style={styles.iconContainer}>
              <Image source={require('./assets/icon_app.png')} style={styles.background_icon}/>
            </View>
          </View>
          <View style={styles.loginContainer}>
            <View>
              <TextInput
                style={this.changeFontInputText()}
                placeholder='Username'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
            <View>
              <TextInput
                style={this.changeFontInputText()}
                placeholder='Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
            <View>
              <TextInput
                style={this.changeFontInputText()}
                placeholder='Re-Password'
                onChangeText={rePassword => this.setState({ rePassword })}
                secureTextEntry={true}
              />
            </View>
            <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>{this.state.errorMsg}</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity style={styles.textButton}>
                <Text style={this.changeFontTextButton()} >Already become Wibu?</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={()=>this.registerHandler()}>
                <Text style={this.changeFontButton()}>{this.props.isUpdate}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#F11F52'
  },
  background:{
    height:DEVICE.height * 0.45,
  },
  background_image:{
    width:'100%', 
    height:DEVICE.height * 0.35,
    marginBottom:-DEVICE.height * 0.1
  },
  iconContainer:{
    alignItems:"center"
  },
  background_icon:{
    width:DEVICE.height * 0.2, 
    height:DEVICE.height * 0.2,
    marginBottom:DEVICE.height * 0.025
  },
  loginContainer:{
    flex:1,
    alignItems:'center',
    marginTop: DEVICE.height * 0.025,
  },
  textInput: {
    width: DEVICE.width*0.9,
    backgroundColor: 'rgba(255, 194, 194, 0.6)',
  },
  buttonContainer:{
    width:DEVICE.width*0.9,
    height:75,
    flexDirection: 'row',
    padding:10,
    justifyContent:'space-between'
  },
  textButton:{
    padding:10,
    alignItems:'center',
    justifyContent:'center'
  },
  button:{
    height:50,
    width: DEVICE.width*0.35,
    backgroundColor:'#F99EBF',
    alignItems:'center',
    justifyContent:'center',
  },
  errorText: {
    color: 'white',
    textAlign: 'center',
    marginBottom: 8,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 8,
  }
})

