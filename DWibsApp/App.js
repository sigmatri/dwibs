import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import * as firebase from 'firebase';
import {firebaseConfig} from './Config';
import {Provider} from 'react-redux';
import {createStore} from'redux';
import reducer from './Screen/reducer';



firebase.initializeApp(firebaseConfig);
// Initialize Firebase
const store = createStore(reducer);
import Index from './Index'
export default function App() {
  return (
    <Provider store={store}>
      <StatusBar/>
      <Index/>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
