import React, { Component } from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import * as Font from 'expo-font';
import {connect} from 'react-redux';

const DEVICE = Dimensions.get('window')
const updateId = (id)=>{
  return{
    type:'LOGGED',
    id:id
  };
}



class LoginScreen extends Component{
  constructor(props){
    super(props);
    this.state={
      fontLoaded:false,
      userName: '',
      password: '',
      isError: false,
      users: [],
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/MontserratAlternates-Regular.ttf'),
    });
    this.setState({ fontLoaded:true});
    this.fetchData();
  }
  // async componentDidUpdate(){
  //   console.log("kepanggil")
  //   if(this.props.isUpdate == true){
  //     this.fetchData();
  //     this.props.dontUpdate;
  //   }
  //   console.log(this.props.isUpdate)
  // }

  fetchData = async()=>{
    console.log("============")
    const response = await fetch('https://dwibs-686cb.firebaseio.com/users.json')
      .then(sresponse=>{
        return sresponse.json();
      })
      .then(users =>{
        console.log(users);
        this.setState({users: users, pleaseUpdate:false});
      })
      .catch(err =>{
        console.log("error----", err);
      })        
      
  }

  loginHandler(){
    console.log(this.state.users);
    console.log(this.state.userName +' '+this.state.password)
    let idUser = null
    if (this.state.users!= null){
      console.log("ayam")
      for(id in this.state.users){
        console.log(id)
        console.log(this.state.users[id])
        if(this.state.users[id]["username"] == this.state.userName){
          idUser = id;
        }
      }
    }
    if (idUser == null){
      console.log("masuk 1")
      this.setState({isError : true});
    }else{
      if (this.state.password == this.state.users[idUser]["password"]){
        //simpen state ke redux
        this.props.updateId(idUser);
        this.props.navigation.navigate('Discover', {userName:this.state.userName})
        console.log("masuk 3")
        console.log("keapdet ga?", this.props.id)
        this.setState({isError : false});
      }else{
        this.setState({isError : true});
        console.log("masuk 2")
      }
    }
  }

  goToRegisterScreen(){
    this.props.navigation.navigate('Register')
  }

  changeFontInputText=()=>{
    if(this.state.fontLoaded){
      return{
        width: DEVICE.width*0.9,
        height: 65,
        padding: 20,
        fontSize: 18,
        fontWeight: '600',
        backgroundColor: 'rgba(255, 194, 194, 0.6)',
        marginVertical:10,
        fontFamily : 'montserrat',
      }
    }else{
      return{
        width: DEVICE.width*0.9,
        height: 65,
        padding: 20,
        fontSize: 18,
        fontWeight: '600',
        backgroundColor: 'rgba(255, 194, 194, 0.6)',
        marginVertical:10,
      }
    }
  }

  changeFontTextButton=()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        fontWeight: '600',
        color:'blue',
        fontFamily : 'montserrat',
      }
    }else{
      return{
        fontSize: 16,
        fontWeight: '600',
      }
    }
  }

  changeFontButton=()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        fontWeight: '700',
        color:'black',
        fontFamily : 'montserrat',
      }
    }else{
      return{
        fontSize: 16,
        fontWeight: '700',
      }
    }
  }

  render(){
    console.log("props ",this.props);
    return(
      <KeyboardAvoidingView style={styles.container} behavior={"height"} enabled>
        <ScrollView>
          <View style={styles.background}>
            <Image source={require('./assets/background.png')} style={styles.background_image}/>
            <View style={styles.iconContainer}>
              <Image source={require('./assets/icon_app.png')} style={styles.background_icon}/>
            </View>
          </View>
          <View style={styles.loginContainer}>
            <View>
              <TextInput
                style={this.changeFontInputText()}
                placeholder='Username'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
            <View>
              <TextInput
                style={this.changeFontInputText()}
                placeholder='Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
            <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password / Username Salah</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity style={styles.textButton}>
                <Text style={this.changeFontTextButton()} >Want to be Wibu?</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => this.loginHandler()}>
                <Text style={this.changeFontButton()}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#F11F52'
  },
  background:{
    height:DEVICE.height * 0.45,
  },
  background_image:{
    width:'100%', 
    height:DEVICE.height * 0.35,
    marginBottom:-DEVICE.height * 0.1
  },
  iconContainer:{
    alignItems:"center"
  },
  background_icon:{
    width:DEVICE.height * 0.2, 
    height:DEVICE.height * 0.2,
    marginBottom:DEVICE.height * 0.05
  },
  loginContainer:{
    flex:1,
    alignItems:'center',
    marginTop: DEVICE.height * 0.05,
  },
  textInput: {
    width: DEVICE.width*0.9,
    backgroundColor: 'rgba(255, 194, 194, 0.6)',
  },
  buttonContainer:{
    width:DEVICE.width*0.9,
    height:75,
    flexDirection: 'row',
    padding:10,
    justifyContent:'space-between'
  },
  textButton:{
    padding:20,
    alignItems:'center',
    justifyContent:'center'
  },
  button:{
    height:50,
    width: DEVICE.width*0.35,
    backgroundColor:'#F99EBF',
    alignItems:'center',
    justifyContent:'center',
  },
  errorText: {
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
})
function mapStateToProps(state){
  return{
    id: state
  }
}
export default connect (mapStateToProps, {updateId})(LoginScreen)