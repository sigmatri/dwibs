import React from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import * as Font from 'expo-font';
import Icon from 'react-native-vector-icons/MaterialIcons';

const DEVICE = Dimensions.get('window')

export default class AnimeMangaItem extends React.Component{
  constructor(){
    super();
    this.state={
      setState: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('../assets/fonts/MontserratAlternates-Regular.ttf'),
      'montserrat-extra-bold': require('../assets/fonts/MontserratAlternates-ExtraBold.ttf'),
      'montserrat-bold': require('../assets/fonts/MontserratAlternates-Bold.ttf'),
      'montserrat-semi-bold': require('../assets/fonts/MontserratAlternates-SemiBold.ttf'),
      'montserrat-medium': require('../assets/fonts/MontserratAlternates-Medium.ttf'),
    });
    this.setState({ fontLoaded:true});
  }

  changeTextStyle = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        fontWeight: '600',
        color:'#281980',
        fontFamily : 'montserrat',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  render(){
    let data = this.props.item; 
    return(
      <View style={styles.container}>
        <Image source={{uri:data.attributes.posterImage.small}} style={styles.animeImage}/>
        <View style={styles.box}>
          <View style={styles.ratingContainer}>
            <Icon name="star" size={15} color="#ffff00" style={{marginTop:1}} />
            <Text style={this.changeTextStyle()}>{data.attributes.averageRating}</Text>
          </View>
          <View style={styles.nameContainer}>
            <Text style={this.changeTextStyle()}>{data.attributes.titles.en_jp}</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    height:DEVICE.height*0.35,
    width:DEVICE.width*0.42,
    backgroundColor:'black',
    margin:4,
    
  },
  box:{
    height:DEVICE.height*0.35,
    width:DEVICE.width*0.42,
    marginTop:-DEVICE.height*0.35,
    justifyContent:'space-between',
    
  },
  animeImage:{
    height: DEVICE.height*0.35,
    width: DEVICE.width*0.42,
    
  },
  ratingContainer:{
    backgroundColor:'rgba(255, 255, 255, 0.8)',
    flexDirection:'row',
    alignSelf:'baseline',
    borderRadius:10,
    paddingLeft:5,
  },
  nameContainer:{
    backgroundColor:'rgba(255, 255, 255, 0.8)',
  }
})