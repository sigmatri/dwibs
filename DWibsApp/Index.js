import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';


import LoginScreen from './Screen/LoginScreen';
import RegisterScreen from './Screen/RegisterScreen';
import Discover from './Screen/Discover';
import Detail from './Screen/Detail';
import Profile from './Screen/Profile';

const Stack = createStackNavigator();

export default class Index extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Register' component={RegisterScreen} options={{headerShown: false}}/>
          <Stack.Screen name='Discover' component={Discover} options={{headerShown: false}}/>
          <Stack.Screen name='Detail' component={Detail} options={{headerShown: false}}/>
          <Stack.Screen name='Profile' component={Profile} options={{headerShown: false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}