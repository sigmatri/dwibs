import React, { Component } from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';
import * as Font from 'expo-font';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Ionicons } from '@expo/vector-icons';

const DEVICE = Dimensions.get('window')

export default class DetailScreen extends Component{
  constructor(props){
    super(props);
    this.state={
      data: [],
      fontLoaded: false,
      pleaseReRender:false,
      title:'',
      rating:'',
      status:'',
      imageUrl:'',
      sinopsis:'',
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/MontserratAlternates-Regular.ttf'),
      'montserrat-extra-bold': require('./assets/fonts/MontserratAlternates-ExtraBold.ttf'),
      'montserrat-bold': require('./assets/fonts/MontserratAlternates-Bold.ttf'),
      'montserrat-semi-bold': require('./assets/fonts/MontserratAlternates-SemiBold.ttf'),
      'montserrat-medium': require('./assets/fonts/MontserratAlternates-Medium.ttf'),
    });
    this.setState({ fontLoaded:true});
    this.fetchData();
  }

  fetchData = async()=>{
    console.log("============")
    console.log(this.props.route.params.id)
    let api = `https://kitsu.io/api/edge/anime/${this.props.route.params.id}`
    const response = await fetch(api)
      .then(sresponse=>{
        return sresponse.json();
      })
      .then(data =>{
        // console.log(data["data"]);
        // console.log(data["data"]["attributes"]["titles"]["en_jp"])
        this.setState({data: data["data"], pleaseReRender:true});
        this.setState({
          title:data["data"]["attributes"]["titles"]["en_jp"],
          rating: data['data']['attributes']['averageRating'],
          status: data['data']['attributes']['status'],
          imageUrl:data['data']['attributes']['posterImage']['medium'],
          sinopsis:data['data']['attributes']['synopsis'],
        })
        console.log(this.state.title)
      })
      .catch(err =>{
        console.log("error----", err);
      })        
  }


  changeHeader = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 24,
        fontWeight: '600',
        color:'#FFFFFF',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 40
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  changeNavText = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#BB86FC',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  changeNavText_1 = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#A9A9A9',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  changeTextStyle = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 16,
        fontWeight: '600',
        color:'#000000',
        fontFamily : 'montserrat',
        marginLeft:5,
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  changeTextStyleSinonpsis = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"white",
        fontSize:16,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  changeTextStyleSinonpsisTittle = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"#FF7597",
        fontSize:32,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  changeTextButton = ()=>{
    if(this.state.fontLoaded){
      return{    
        fontFamily:'montserrat',
        color:"black",
        fontSize:24,
        textAlign:'justify'
      }
    }else{
      return{
        fontSize: 16,
      }
    }
  }

  render(){
    return(
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.TabBar}>
            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
              <Ionicons name="ios-arrow-back" size={25} color="white" style={{marginLeft:20}} />
            </TouchableOpacity>
            <View style={{flexDirection:'row', flex:1, justifyContent:'center', marginLeft:-15}}>
              <Text style={this.changeHeader()}>{this.state.title}</Text>
            </View>
          </View>
          <View style={styles.ImageItem}>
            <Image source={{uri:this.state.imageUrl}} style={styles.posterImage}/>
          </View>
          <View style={styles.boxDetail}>
            <View style={{flexDirection:'row'}}>
              <View style={styles.ratingContainer}>
                <Icon name="star" size={15} color="#ffff00" style={{marginTop:1.5}} />
                <Text style={this.changeTextStyle()}>{this.state.rating}</Text>
              </View>
              <View style={styles.statusContainer}>
                <Text style={this.changeTextStyle()}>{this.state.status}</Text>
              </View>
            </View>
            <View style={styles.sinopsisContainer}>
              <Text style={this.changeTextStyleSinonpsisTittle()}>Sinopsis</Text>
              <View style={styles.underLine}/>
              <Text style={this.changeTextStyleSinonpsis()}>{this.state.sinopsis}</Text>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.addToPlan} onPress={()=>{alert("belum di implementasi")}}>
              <Text style={this.changeTextButton()}>Add to Plan</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.watch} onPress={()=>{alert("belum di implementasi")}}>
              <Text style={this.changeTextButton()}>Watch</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#121212'
  },
  TabBar:{
    height:38,
    backgroundColor:"#1f1f1f",
    flexDirection:'row',
    alignItems:'center',
    marginTop:15,
    elevation:1,
  },
  ImageItem:{
    height: 300,
    alignItems:'center',
    marginTop:5,
  },
  posterImage:{
    width:DEVICE.width*0.9,
    height: 300,
    resizeMode:"contain",
  },
  boxDetail:{
    justifyContent:'center',
    margin: 5,
    borderRadius:5,    
    backgroundColor:'#1F1F1F',
    padding:10,
  },
  ratingContainer:{
    flexDirection:'row',
    backgroundColor:'#BB86FC',
    alignSelf:'baseline',
    borderRadius:10,
    paddingHorizontal:10,
  },
  statusContainer:{
    alignSelf:'baseline',
    paddingRight:10,
    paddingLeft:5,
    marginLeft:10,
    backgroundColor:'#03DAC5',
    borderRadius:10,
  },
  underLine:{
    height:3,
    width:65,
    backgroundColor:"#BB86FC",
    borderRadius:1.5,
    marginTop:5,
    marginBottom:10
  },
  addToPlan:{
    backgroundColor:'#BB86FC',
    margin:5,
    height:64,
    justifyContent:'center',
    alignItems:'center',
  },
  watch:{
    backgroundColor:'#03DAC5',
    margin:5,
    height:64,
    justifyContent:'center',
    alignItems:'center',
  }
})