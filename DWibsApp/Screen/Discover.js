import React, { Component } from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';
import * as Font from 'expo-font';
import Icon from 'react-native-vector-icons/MaterialIcons';

import AnimeManga from './components/AnimeMangaItem'

const DEVICE = Dimensions.get('window')

export default class DiscoverScreen extends Component{
  constructor(props){
    super(props);
    this.state={
      datas:[],
      fontLoaded: false,
      pleaseReRender:false,
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/MontserratAlternates-Regular.ttf'),
      'montserrat-extra-bold': require('./assets/fonts/MontserratAlternates-ExtraBold.ttf'),
      'montserrat-bold': require('./assets/fonts/MontserratAlternates-Bold.ttf'),
      'montserrat-semi-bold': require('./assets/fonts/MontserratAlternates-SemiBold.ttf'),
      'montserrat-medium': require('./assets/fonts/MontserratAlternates-Medium.ttf'),
    });
    this.setState({ fontLoaded:true});
    this.fetchData();
  }

  fetchData = async()=>{
    console.log("============")
    const response = await fetch('https://kitsu.io/api/edge/trending/anime')
      .then(sresponse=>{
        return sresponse.json();
      })
      .then(datas =>{
        console.log(datas);
        this.setState({datas: datas, pleaseReRender:true});
      })
      .catch(err =>{
        console.log("error----", err);
      })        
  }
  changeHeader = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 24,
        fontWeight: '600',
        color:'#FFFFFF',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 20,
        fontWeight: '700',
      }
    }
  }
  changeNavText = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#BB86FC',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  changeNavText_1 = ()=>{
    if(this.state.fontLoaded){
      return{
        fontSize: 38,
        fontWeight: '600',
        color:'#A9A9A9',
        fontFamily : 'montserrat-medium',
        marginHorizontal: 10
      }
    }else{
      return{
        fontSize: 38,
        fontWeight: '700',
      }
    }
  }

  goToDetail=(id)=>{
    this.props.navigation.navigate('Detail', {id:id})
  }
  listItem=()=>{
    return (
    <FlatList 
      data={this.state.datas.data}
      renderItem={({item})=>{
        return(
        <TouchableOpacity onPress={()=>{this.goToDetail(item.id)}}>
          <AnimeManga item={item}/>  
        </TouchableOpacity>
      )
    }}
      keyExtractor={(item)=>item.id}
      numColumns={2}
    />
  )}
  render(){
    let listAnimeManga;
    if (this.state.pleaseReRender) {
      listAnimeManga = this.listItem();      
    } else {
      listAnimeManga = <View style={styles.notFoundContainer}>
        <Image source={require('./assets/not_found.jpg')} style={styles.not_found}/>
      </View>
      
    }
    return(
      <View style={styles.container}>
        <View style={styles.TabBar}>
          <View style={{flexDirection:'row'}}>
            <TouchableOpacity style={{marginLeft:15, marginTop:3}}>
              <Icon name="search" size={25} color="#FFFFFF" />
            </TouchableOpacity>
            <Text style={this.changeHeader()}>Discover</Text>
          </View>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Profile')}}>
            <Image source={require('./assets/pp.png')} style={styles.pp}/>
          </TouchableOpacity>
        </View>
        <View style={styles.TabNavigator}>
          <TouchableOpacity>
            <Text style={this.changeNavText()}>Anime</Text>
            <View style={styles.underLine}/>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={this.changeNavText_1()}>Manga</Text>
            {/* <View style={styles.underLine}/> */}
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          {listAnimeManga}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#121212'
  },
  TabBar:{
    height:38,
    backgroundColor:"#1f1f1f",
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  pp:{
    height:25,
    width:25,
    borderRadius:5,
    marginHorizontal:20,
  },
  not_found:{
    width: undefined,
    height: '100%',
    aspectRatio: 0.5,
  },
  notFoundContainer:{
    backgroundColor:"#121212",
    alignItems:'center'
  },
  body:{
    flex:1,
    alignItems:'center'
  },
  TabNavigator:{
    marginHorizontal:20,
    marginVertical:4,
    flexDirection:'row'
  },
  underLine:{
    height:3,
    width:65,
    backgroundColor:"#BB86FC",
    marginHorizontal:20,
    borderRadius:1.5
  }
})